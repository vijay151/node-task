module.exports = {
  responseMessage(status, status_code, message) {
    return {
      status: status,
      status_code: status_code,
      message: message,
    };
  },
  responseWithData(status, status_code, message, data) {
    return {
      status: status,
      status_code: status_code,
      message: message,
      data: data,
    };
  },
};
