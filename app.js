const express = require("express");
const app = express();
const cors = require("cors");
const cookieParser = require("cookie-parser");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
require("dotenv/config");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(cors());

mongoose
  .connect("mongodb://127.0.0.1:27017/Task", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then((db) => {
    console.log("mongodb connected successfully");
  })
  .catch((err) => {
    console.log("mongodb error", err);
  });

app.listen("8080", async (err) => {
  if (!err) {
    console.log("server start on 8080");
  }
});

// routes

const Task = require("./routes/route");

app.use("/api", Task);
