const mongoose = require("mongoose");

const Password = mongoose.Schema(
  {
    password: {
      type: String,
      required: true,
    },
  },
  { versionKey: false }
);

module.exports = mongoose.model("Password", Password);
