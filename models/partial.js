const mongoose = require("mongoose");

const Partial = mongoose.Schema(
  {
    array: {
      type: Array,
      required: true,
    },
    combination: {
      type: String,
      required: true,
    },
    output: {
      type: Number,
      required: true,
    },
  },
  { versionKey: false }
);

module.exports = mongoose.model("Partial", Partial);
