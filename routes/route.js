const { all, password, partial } = require("../controller/Task");
const express = require("express");
const router = express.Router();

const Routes = [
  {
    method: "post",
    route: "/password/create",
    action: password,
  },
  {
    method: "post",
    route: "/partial/create",

    action: partial,
  },
  {
    method: "get",
    route: "/list",

    action: all,
  },
];

Routes.forEach((e) => {
  router[e.method](e.route, e.action);
});

module.exports = router;
