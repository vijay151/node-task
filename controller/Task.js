const responseMessage = require("../configs/response");
// const msg = require("../configs/message");
const Password = require("../models/password");
const Partial = require("../models/partial");

// create password

const password = async (req, res, next) => {
  const password = new Password({
    password: req.body.password,
  });

  await password
    .save()
    .then((data) => {
      return res
        .status(200)
        .send(
          responseMessage.responseWithData(
            true,
            200,
            "password create successfully",
            data
          )
        );
    })
    .catch((err) => {
      return res
        .status(400)
        .send(
          responseMessage.responseWithData(
            false,
            400,
            "password create failed",
            err
          )
        );
    });
};

// create partial

const partial = async (req, res, next) => {
  try {
    if (!req.body.array || req.body.array.length < 2) {
      return res
        .status(400)
        .send(
          responseMessage.responseWithData(
            false,
            400,
            "array is required minimum length 2",
            err
          )
        );
    }
    const arr = req.body.array.map((e) => Number(e));

    const middle = arr.length / 2;

    // store   combination of array

    let c = [];
    let d = [];
    let value;

    let combinations = [];

    // split two array
    const a = arr.slice(0, middle);
    const b = arr.slice(middle);

    const checkMinimizeValue = () => {
      for (i = 0; i < a.length; i++) {
        for (let j = 0; j < b.length; j++) {
          c = [a[j], ...b.slice(0, i), ...b.slice(i + 1)];
          d = [b[i], ...a.slice(0, j), ...a.slice(j + 1)];

          //   some two array
          const val = c.reduce((a, b) => a + b) - d.reduce((a, b) => a + b);

          //  if check array even number or add number
          if (arr.length % 2 === 0) {
            if (value > val) {
              if (val >= 0) {
                value = val;
                combinations = [c, d];
              } else {
                value = value;
              }
            }
          } else {
            // add number calculation
            if (value > val) {
              if (val >= 0) {
                value = val;
                combinations = [c, d];
              } else {
                value = value;
              }
            } else {
              if (val === 0) {
                value = val;
                combinations = [c, d];
              }
              if (value < 0) {
                if (val > 0) {
                  value = val;
                  combinations = [c, d];
                }
              }
            }
          }
        }
      }
    };

    // check value is exist

    if (value) {
      checkMinimizeValue();
    } else {
      value = a.reduce((a, b) => a + b) - b.reduce((a, b) => a + b);
      combinations = [[...a], [...b]];
    }

    // value added after call function
    if (value) {
      checkMinimizeValue();
    }

    const partial = new Partial({
      array: arr,
      combination: JSON.stringify(combinations),
      output: value,
    });
    await partial
      .save()
      .then((data) => {
        return res
          .status(200)
          .send(
            responseMessage.responseWithData(
              true,
              200,
              "partial create successfully",
              data
            )
          );
      })
      .catch((err) => {
        return res
          .status(400)
          .send(
            responseMessage.responseWithData(
              false,
              400,
              "partial create failed",
              err
            )
          );
      });
  } catch (err) {
    return res
      .status(400)
      .send(
        responseMessage.responseWithData(
          false,
          400,
          "password create successfully",
          err
        )
      );
  }
};

// list data
const all = async (req, res, next) => {
  try {
    const partial = await Partial.find();
    const password = await Password.find();
    const data = {
      partial,
      password,
    };
    return res
      .status(200)
      .send(
        responseMessage.responseWithData(
          true,
          200,
          "data list successfully",
          data
        )
      );
  } catch (error) {
    return res
      .status(400)
      .send(
        responseMessage.responseWithData(false, 400, "data list failed", err)
      );
  }
};

module.exports = {
  all,
  partial,
  password,
};
